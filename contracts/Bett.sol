pragma solidity ^0.5.0;

import "./TRC20.sol";

contract Bett is TRC20 {
    struct LockedToken {
        uint256 amount;
        uint32  unlockDeadline;
    }

    event TokenLocked(
        address indexed from,
        address indexed recepient,
        uint256 amount,
        uint32  deadline);

    event TokenUnlocked(
        address indexed recepient,
        uint256 amount,
        uint32  deadline
    );

    mapping(address => LockedToken) lockedTokens;

    constructor (address recepient) public TRC20("Bett token", "BETT", 8) {
        require(recepient != address(0), "can not deploy to address 0");
        _mint(recepient, 350000000 * (10 ** uint256(decimals()))); // 3.5 亿
    }

    function rand(uint256 key) public view returns(uint32 r) {
        address p1 = msg.sender;
        uint256 p2 = balanceOf(p1);
        uint256 p3 = block.number;
        uint256 p4 = block.timestamp;

        bytes memory v = abi.encode(p1, p2, key, p3, p4);

        return uint32(uint256(keccak256(v)) % 6 + 1);
    }

    function lock(address beneficiary, uint256 amount, uint32 locktype) external {

        uint32 deadline;
        if (locktype == 0)
            deadline = uint32(block.timestamp + 7 days);
        else if (locktype == 1)
            deadline = uint32(block.timestamp + 30 days);
        else if (locktype == 2)
            deadline = uint32(block.timestamp + 365 days);
        else
            require(false, "locktype can only be 0, 1 or 2");

        _transfer(msg.sender, address(this), amount);

        LockedToken storage locked = lockedTokens[beneficiary];
        locked.amount = amount.add(locked.amount);
        locked.unlockDeadline = deadline;

        emit TokenLocked(msg.sender, beneficiary, amount, deadline);
    }

    function unlock() external {
        uint256 amount = lockedTokens[msg.sender].amount;
        uint32 deadline = lockedTokens[msg.sender].unlockDeadline;

        require(amount > 0, "No Locked Token Exist");
        require(deadline <= block.timestamp, "Can Not Unlock Now.");

        _transfer(address(this), msg.sender, amount);

        delete lockedTokens[msg.sender];

        emit TokenUnlocked(msg.sender, amount, deadline);
    }

    function totalLocked() external view returns(uint256) {
        return balanceOf(address(this));
    }
}